﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using WpfApp1.Models;

namespace WpfApp1.Services
{
    public class JsonFileService 
    {
        public DbModel Open(string fileName)
        {
            DbModel model;

            using (FileStream reader = new FileStream(fileName, FileMode.Open, FileAccess.Read))
            {
                DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(DbModel));
                model = (DbModel)ser.ReadObject(reader);
            }

            return model;
        }

        public void Save(string fileName, DbModel model)
        {
            var serializer = new DataContractJsonSerializer(typeof(DbModel));

            using (FileStream fs = new FileStream(fileName, FileMode.Create))
            {
                using (var writer = JsonReaderWriterFactory.CreateJsonWriter(fs, Encoding.UTF8, true, true, "  "))
                {
                    serializer.WriteObject(writer, model);
                }
            }
        }
    }
}
