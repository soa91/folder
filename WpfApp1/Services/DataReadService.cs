﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using WpfApp1.Models;

namespace WpfApp1.Services
{
    public class DataReadService
    {
        private readonly string _fullPathConfig;
        private readonly JsonFileService _jsonFileService;
        public DataReadService()
        {
            _fullPathConfig = Environment.CurrentDirectory + @"\\config.json";
            _jsonFileService = new JsonFileService();
        }

        public void Open()
        {
            DbModel.SetInstance(_jsonFileService.Open(_fullPathConfig));
        }

        public void Save()
        {
            var instans = DbModel.GetInstance();

            _jsonFileService.Save(_fullPathConfig, instans);
        }
    }
}
