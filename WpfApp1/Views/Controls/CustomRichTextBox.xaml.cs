﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp1.Views.Controls
{
    /// <summary>
    /// Interaction logic for CustomRichTextBox.xaml
    /// </summary>
    public partial class CustomRichTextBox : UserControl
    {
        public static readonly DependencyProperty TextSelectedProperty =
            DependencyProperty.Register("TextSelected", typeof(TextSelection), typeof(CustomRichTextBox));
        public static readonly DependencyProperty SelectionStartProperty =
            DependencyProperty.Register("SelectionStart", typeof(int), typeof(CustomRichTextBox));
        public static readonly DependencyProperty SelectionEndProperty =
            DependencyProperty.Register("SelectionEnd", typeof(int), typeof(CustomRichTextBox));
        public static readonly DependencyProperty MultiColorTextProperty =
            DependencyProperty.Register("MultiColorText", typeof(Paragraph), typeof(CustomRichTextBox), 
                new PropertyMetadata(null, new PropertyChangedCallback(OnMultiColorTextChange)));
        
        public int SelectionStart
        {
            get { return (int) GetValue(SelectionStartProperty); }
            set { SetValue(SelectionStartProperty, value); }
        }

        public int SelectionEnd
        {
            get { return (int)GetValue(SelectionEndProperty); }
            set { SetValue(SelectionEndProperty, value); }
        }

        public Paragraph MultiColorText
        {
            get { return (Paragraph) GetValue(MultiColorTextProperty); }
            set { SetValue(MultiColorTextProperty, value); }
        }

        public TextSelection TextSelected
        {
            get { return (TextSelection)GetValue(TextSelectedProperty); }
            set { SetValue(TextSelectedProperty, value); }
        }

        public CustomRichTextBox()
        {
            InitializeComponent();
        }

        private static void OnMultiColorTextChange(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            CustomRichTextBox c = d as CustomRichTextBox;
            if (c != null) c.OnCustomRichTextBoxChanged();
        }

        private void OnCustomRichTextBoxChanged()
        {
            RichTextBox.Document.Blocks.Clear();
            RichTextBox.Document.Blocks.Add(MultiColorText);
        }

        private void MultiColorText_SourceUpdated(object sender, DataTransferEventArgs e)
        {
            RichTextBox.Document.Blocks.Clear();
            RichTextBox.Document.Blocks.Add(MultiColorText);
        }
        
        private void RichTextBox_OnSelectionChanged(object sender, RoutedEventArgs e)
        {
            TextSelected = RichTextBox.Selection;

            var docStart = RichTextBox.Document.ContentStart;

            var selectionStart = RichTextBox.Selection.Start;
            var selectionEnd = RichTextBox.Selection.End;

            var indexStart = docStart.GetOffsetToPosition(selectionStart);
            var indexEnd = docStart.GetOffsetToPosition(selectionEnd);

            TextRange startTr = new TextRange(docStart, selectionStart);
            TextRange endTr = new TextRange(docStart, selectionEnd);

            int indexStartAbs = startTr.Text.Length;
            int indexEndAbs = endTr.Text.Length;

            SelectionStart = indexStartAbs;
            SelectionEnd = indexEndAbs;
        }

        private void RichTextBox_OnSourceUpdated(object sender, DataTransferEventArgs e)
        {
            RichTextBox.Document.Blocks.Clear();
            RichTextBox.Document.Blocks.Add(MultiColorText);
        }
    }
}
