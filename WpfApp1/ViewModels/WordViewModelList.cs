﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using WpfApp1.Models;

namespace WpfApp1.ViewModels
{
    public class WordViewModelList : ObservableCollection<WordViewModel>
    {
        public WordViewModelList(DbModel model)
        {
            var list = model.Words.GetAll().ToList();
            list.Sort((a, b) => a.Word.CompareTo(b.Word));

            foreach (var wordEntity in list)
            {
                Add(new WordViewModel(wordEntity));
            }
        }
    }
}
