﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using WpfApp1.Infrastructure;
using WpfApp1.Models;
using WpfApp1.Services;

namespace WpfApp1.ViewModels
{
    public class AppViewModel : ViewModel
    {
        private DataReadService _dataReadService;
        public Command LoadCommand { get; }
        public Command SaveCommand { get; }

        private WordViewModelList _list;
        public WordViewModelList List
        {
            get { return _list; }
            set
            {
                _list = value;
                NotifyPropertyChanged();
            }
        }

        public int SelectedElementInd { get; set; }

        public AppViewModel(DataReadService dataReadService)
        {
            _dataReadService = dataReadService;

            LoadCommand = new Command(Load);
            SaveCommand = new Command(Save);

            List = new WordViewModelList(DbModel.GetInstance());
            //var list = new ObservableCollection<Word>();

            //list.Add(new Word(new List<WordPart>
            //{
            //    new WordPart("asdaasda", Brushes.White, Brushes.Black),
            //    new WordPart("asdaasda", Brushes.Black, Brushes.Green)

            //}));
            //list.Add(new Word(new List<WordPart> {new WordPart("ssssssss", Brushes.White, Brushes.Black)}));
            //list.Add(new Word(new List<WordPart> {new WordPart("qqqqqqqq", Brushes.White, Brushes.Black)}));

            //List = list;
        }
        
        public void Load()
        {
            _dataReadService.Open();
            List = new WordViewModelList(DbModel.GetInstance());
        }

        public void Save()
        {
            _dataReadService.Save();
        }

        //private void Update()
        //{
        //    List = LoadWordList();
        //}

        //private ObservableCollection<WordViewModel> LoadWordList()
        //{
        //    var list = new ObservableCollection<WordViewModel>();

        //    foreach (var wordEntity in DbModel.GetInstance().Words)
        //    {
                
        //    }

        //    return list;
        //}
    }
}
