﻿using System.Collections.Generic;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Markup;
using WpfApp1.Infrastructure;
using WpfApp1.Models;
using WpfApp1.Views;

namespace WpfApp1.ViewModels
{
    public class WordViewModel : ViewModel
    {
        public WordEntity _word;
        public Paragraph Tb
        {
            get
            {
                var tb = new Paragraph();

                if (string.IsNullOrWhiteSpace(_word.ColoredXmlText))
                {
                    tb.Inlines.Add(new Run()
                    {
                        Text = _word.Word
                    });
                }
                else
                {
                    tb = (Paragraph)XamlReader.Parse(_word.ColoredXmlText);
                }

                return tb;
            }
            set { }
        }

        public Command OpenEditWindowCommand { get; }

        public WordViewModel(WordEntity word)
        {
            _word = word;

            OpenEditWindowCommand = new Command(Edit);
        }

        private void Edit()
        {
            var evm = new EditViewModel(_word);

            var ev = new EditView()
            {
                DataContext = evm,
            };

            ev.ShowDialog();
            NotifyPropertyChanged("Tb");
        }
    }
}