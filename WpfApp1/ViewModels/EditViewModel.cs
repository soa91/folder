﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Markup;
using System.Windows.Media;
using WpfApp1.Infrastructure;
using WpfApp1.Models;

namespace WpfApp1.ViewModels
{
    public class EditViewModel : ViewModel
    {
        public Command ClickRedCommand { get; }
        public Command ClickOrangeCommand { get; }
        public Command ClickYellowCommand { get; }
        public Command ClickGreenCommand { get; }
        public Command ClickLightBlueCommand { get; }
        public Command ClickBlueCommand { get; }
        public Command ClickPurpleCommand { get; }

        public Command ClickSaveCommand { get; }


        private WordEntity _word;

        private Paragraph _tb;

        public Paragraph Tb
        {
            get { return _tb; }
            set
            {
                _tb = value;
                NotifyPropertyChanged();
            }
        }

        public TextSelection SelText { get; set; }
        public int SelStart { get; set; }
        public int SelEnd { get; set; }

        public EditViewModel(WordEntity word)
        {
            _word = word;

            ClickRedCommand = new Command(ClickRed);
            ClickOrangeCommand = new Command(ClickOrange);
            ClickYellowCommand = new Command(ClickYellow);
            ClickGreenCommand = new Command(ClickGreen);
            ClickLightBlueCommand = new Command(ClickLightBlue);
            ClickBlueCommand = new Command(ClickBlue);
            ClickPurpleCommand = new Command(ClickPurple);

            ClickSaveCommand = new Command(ClickSave);

            var par = new Paragraph();

            if (string.IsNullOrWhiteSpace(_word.ColoredXmlText))
            {
                par.Inlines.Add(new Run()
                {
                    Text = _word.Word
                });
            }
            else
            {
                par = (Paragraph)XamlReader.Parse(_word.ColoredXmlText);
            }

            Tb = par;
        }

        #region Methods for Commands
        private void ClickSave()
        {
            _word.ColoredXmlText = XamlWriter.Save(Tb);

            DbModel.GetInstance().Words.Update(_word);
        }

        private void ClickRed()
        {
            Click(Brushes.Red);
        }
        private void ClickOrange()
        {
            Click(Brushes.Orange);
        }

        private void ClickYellow()
        {
            Click(Brushes.Yellow);
        }

        private void ClickGreen()
        {
            Click(Brushes.Green);
        }

        private void ClickLightBlue()
        {
            Click(Brushes.LightBlue);
        }

        private void ClickBlue()
        {
            Click(Brushes.Blue);
        }

        private void ClickPurple()
        {
            Click(Brushes.Purple);
        }

        private void Click(Brush color)
        {
            TextRange tr = new TextRange(SelText.Start, SelText.End);

            tr.ApplyPropertyValue(TextElement.ForegroundProperty, color);
        }
        #endregion
    }
}
