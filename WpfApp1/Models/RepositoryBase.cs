﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Serialization;

namespace WpfApp1.Models
{
    [DataContract]
    [KnownType(typeof(RepositoryBase<WordEntity>))]
    public class RepositoryBase<TEntity> where TEntity : PersistentObjectBase
    {
        [DataMember]
        private List<TEntity> _list;
        [DataMember]
        private long _inc;

        public RepositoryBase()
        {
            _list = new List<TEntity>();
            _inc = 0;
        }

        public void Add(TEntity entity)
        {
            if (entity == null) return;
            if (!entity.IsNew()) return;

            entity.Id = _inc;
            
            _list.Add(entity);
            _inc++;
        }

        public void Update(TEntity entity)
        {
            if (entity == null) return;
            if (entity.IsNew()) return;

            var found = _list.FirstOrDefault(a => Equals(a.Id, entity.Id));

            if (found == null) return;

            var id = found.Id;

            Delete(found);

            entity.Id = id;
            _list.Add(entity);
        }

        public void Delete(TEntity entity)
        {
            var found = _list.FirstOrDefault(a => Equals(a.Id, entity.Id));

            if (found == null) return;

            _list.Remove(found);
        }

        public TEntity Get(long id)
        {
            return _list.FirstOrDefault(a => Equals(a.Id, id));
        }

        public TEntity GetLastAdd()
        {
            return Get(_inc - 1);
        }

        public IQueryable<TEntity> GetAll()
        {
            return _list.AsQueryable();
        }

        public TEntity Get(Expression<Func<TEntity, bool>> where)
        {
            return _list.AsQueryable().FirstOrDefault(where);
        }

        public IEnumerable<TEntity> GetMany(Expression<Func<TEntity, bool>> @where)
        {
            return _list.AsQueryable().Where(where);
        }
    }
}
