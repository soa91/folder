﻿using System.Runtime.Serialization;

namespace WpfApp1.Models
{
    [DataContract]
    [KnownType(typeof(WordEntity))]
    public abstract class PersistentObjectBase 
    {
        protected PersistentObjectBase()
        {
            Id = -1;
        }

        /// <summary>
        /// Внутреннее имя (ключ), исопльзуемое для адресации и отношений в алгоритмах.
        /// Внутри запросов и на схеме не исопльзуется.
        /// </summary>
        [DataMember]
        public long Id { get; set; }


        public bool IsNew()
        {
            return Id == -1;
        }
    }
}