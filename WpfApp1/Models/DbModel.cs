﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Windows.Documents;

namespace WpfApp1.Models
{
    [DataContract]
    public class DbModel
    {
        private static DbModel _dbReps = new DbModel();

        [DataMember]
        public RepositoryBase<WordEntity> Words = new RepositoryBase<WordEntity>();

        public static DbModel GetInstance()
        {
            return _dbReps;
        }

        public static void SetInstance(DbModel reps)
        {
            _dbReps = reps;
        }
    }
}
