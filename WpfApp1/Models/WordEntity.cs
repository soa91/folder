﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace WpfApp1.Models
{
    [DataContract]
    public class WordEntity : PersistentObjectBase
    {
        [DataMember]
        public string Word { get; set; }
        [DataMember] 
        public string ColoredXmlText { get; set; }
    }
}
