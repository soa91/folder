﻿using System.Collections.Generic;
using System.Windows;
using WpfApp1.Models;
using WpfApp1.Services;
using WpfApp1.ViewModels;
using WpfApp1.Views;

namespace WpfApp1
{
    /// <summary>
    /// Логика взаимодействия для App.xaml
    /// </summary>
    public partial class App : Application
    {
        private DataReadService _dataReadService;

        public App()
        {
            _dataReadService = new DataReadService();

            GenerateTestList();
            
            var mv = new AppView()
            {
                DataContext = new AppViewModel(_dataReadService)
            };

            mv.Show();
        }

        private void GenerateTestList()
        {
            var words = DbModel.GetInstance().Words;
            
            words.Add(new WordEntity()
            {
                Word = "0123456789"
            });

            words.Add(new WordEntity()
            {
                Word = "bbbbbbbbbbbbbbb"
            });

            words.Add(new WordEntity()
            {
                Word = "ccccccccccccccccc"
            });

            words.Add(new WordEntity()
            {
                Word = "ddddddddddddddddd"
            });

            words.Add(new WordEntity()
            {
                Word = "eeeeeeeeeeeeeee"
            });

            words.Add(new WordEntity()
            {
                Word = "ffffffffffffffff"
            });

            words.Add(new WordEntity()
            {
                Word = "gggggggggggggggg"
            });
        }
    }
}
